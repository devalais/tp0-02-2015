package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

/**
 * Created by root on 05/09/15.
 */
public class ModOperator implements Operator {
    public void operate(OperandsStack operandsStack, String expressionElement) {
        OperandHelper.operate(operandsStack, new SingleOperation() {
            @Override
            public Float operate(Float anOperand, Float anOtherOperand) {
                return anOperand % anOtherOperand;
            }
        });
    }
}
