package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

/**
 * Created by ezequiel on 30/08/15.
 */
public class MultiplicationOperator implements Operator {
    public void operate(OperandsStack operandsStack, String expressionElement) {
        OperandHelper.operate(operandsStack, new SingleOperation() {
            @Override
            public Float operate(Float anOperand, Float anOtherOperand) {
                return anOperand * anOtherOperand;
            }
        });
    }
}
