package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

/**
 * Created by ezequiel on 03/09/15.
 */
public class NumericOperator implements Operator {
    @Override
    public void operate(OperandsStack operandsStack, String expressionElement) {
        operandsStack.add(Float.parseFloat(expressionElement));
    }
}
