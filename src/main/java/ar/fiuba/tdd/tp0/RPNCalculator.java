package ar.fiuba.tdd.tp0;

import ar.fiuba.tdd.tp0.operators.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RPNCalculator {
    private static final java.lang.String SEPARATOR = " ";
    private Map<String, String> operatorsFilter;
    private Map<String, Operator> operatorsMapping;

    static final String addition = "+";
    static final String substraction = "-";
    static final String multiplication = "*";
    static final String divition = "/";
    static final String multiMultiplication = "**";
    static final String multiAddition = "++";
    static final String multiSubstract = "--";
    static final String multiDivition = "//";
    static final String mod = "MOD";

    public RPNCalculator() {
        operatorsFilter = getOperatorsFilter();
        operatorsMapping = getOperatorsMapping();
    }

    public float eval(String expression) {
        // remove try catch
        try {
            Objects.requireNonNull(expression);
        } catch (Exception e) {
            throw new IllegalArgumentException("Empty expression");
        }
        String[] expressionElements = expression.split(SEPARATOR);
        OperandsStack operandsStack = new OperandsStack();
        for (String expressionElement : expressionElements) {
            Operator operator = this.getOperator(expressionElement);
            operator.operate(operandsStack, expressionElement);
        }
        return operandsStack.pop();
    }

    private Operator getOperator(String expressionElement) {
        String operatorIdentifier = operatorsFilter.get(expressionElement);
        Operator operator = operatorsMapping.get(operatorIdentifier);
        return operator;

    }

    private Map<String, String> getOperatorsFilter() {
        return new HashMap<String, String>()
            { {
                    put(addition, addition);
                    put(substraction, substraction);
                    put(multiplication, multiplication);
                    put(divition, divition);
                    put(multiMultiplication, multiMultiplication);
                    put(multiAddition, multiAddition);
                    put(multiSubstract, multiSubstract);
                    put(multiDivition, multiDivition);
                    put(mod, mod);
                } };
    }

    private Map<String, Operator> getOperatorsMapping() {
        return new HashMap<String, Operator>()
            { {
                    put(null, new NumericOperator());
                    put(addition, new AdditionOperator());
                    put(substraction, new SubstractionOperator());
                    put(multiplication, new MultiplicationOperator());
                    put(divition, new DivitionOperator());
                    put(multiMultiplication, new MultiMultiplicationOperator());
                    put(multiAddition, new MultiAdditionOperator());
                    put(multiSubstract, new MultiSubstractOperator());
                    put(multiDivition, new MultiDivitionOperator());
                    put(mod, new ModOperator());
                } };
    }
}