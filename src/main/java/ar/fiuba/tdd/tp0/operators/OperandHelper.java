package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

import java.util.List;

/**
 * Created by root on 05/09/15.
 */
public class OperandHelper {

    static void multiOperate(OperandsStack operandsStack, SingleOperation operation) {
        //TODO remove try catch
        try {
            List<Float> operands = operandsStack.popToList();
            Float result = operands.remove(0);

            for (Float operand : operands) {
                result = operation.operate(result, operand);
            }
            operandsStack.add(result);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }

    static void operate(OperandsStack operandsStack, SingleOperation operation) {
        //TODO remove try catch
        try {
            Float anElement = operandsStack.pop();
            Float anOtherElement = operandsStack.pop();
            Float result = operation.operate(anOtherElement, anElement);
            operandsStack.add(result);
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
