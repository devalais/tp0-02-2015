package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

/**
 * Created by ezequiel on 30/08/15.
 */
public interface Operator {
    void operate(OperandsStack operandsStack, String expressionElement);
}
