package ar.fiuba.tdd.tp0.operators;

/**
 * Created by root on 05/09/15.
 */
public interface SingleOperation {
    Float operate(Float anOperand, Float anOtherOperand);
}
