package ar.fiuba.tdd.tp0.operators;

import ar.fiuba.tdd.tp0.OperandsStack;

/**
 * Created by ezequiel on 04/09/15.
 */
public class MultiAdditionOperator implements Operator {
    @Override
    public void operate(OperandsStack operandsStack, String expressionElement) {
        OperandHelper.multiOperate(operandsStack, new SingleOperation() {
            @Override
            public Float operate(Float anOperand, Float anOtherOperand) {
                return anOperand + anOtherOperand;
            }
        });
    }
}
