package ar.fiuba.tdd.tp0;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by ezequiel on 30/08/15.
 */
public class OperandsStack {
    Stack<Float> stack = new Stack<Float>();

    public Float pop() {
        return stack.pop();
    }

    public void add(Float number) {
        stack.push(number);
    }

    public Integer getSize() {
        return stack.size();
    }

    public List<Float> popToList() {
        List<Float> stackToList = new ArrayList<Float>(stack);
        stack.clear();
        return stackToList;
    }
}
